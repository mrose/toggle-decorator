import equals from 'ramda/src/equals';
import gt from 'ramda/src/gt';
import gte from 'ramda/src/gte';
import has from 'ramda/src/has';

import { createToggleFactoryConfig } from './configure';
import {
  ConfigArrArgs,
  ConfigOptionsArgs,
  Target,
  ToggleFactoryOptions,
  ToggleObj,
} from './toggle.d';
import { validateMax, validateMin, validateTarget } from './validators';

export function toggle(...args: ConfigArrArgs | ConfigOptionsArgs) {
  // { defaultValue, max, min, name, oSet, ...rest }
  const config: ToggleFactoryOptions = createToggleFactoryConfig(...args);

  let decorate = function decorate(target: Target): Target {
    validateTarget(target);

    const udpName = `__${config.name}Toggle`;

    // when the config.name key exists on the target,
    // use the target's value, or default
    const memento = has(config.name, target)
      ? target[config.name]
      : config.defaultValue;

    const toggleObj: ToggleObj = {
      __key: Symbol('key'),

      get isToggled(): boolean {
        return config.oSet.has(toggleObj.__key);
      },
      get memento(): unknown {
        return memento;
      },
      set memento(m: unknown) {
        throw new Error('memento cannot be reset');
      },
      get max(): number {
        return config.max;
      },
      get min(): number {
        return config.min;
      },
      get size(): number {
        return config.oSet.size;
      },
      toggle: function (force?: boolean): boolean {
        if (equals(0, config.max)) return toggleObj.isToggled;
        if (equals(1, config.max) && equals(1, config.min)) return replace();
        return range(force);
      },
    };

    // update the descriptor for the target's key value
    Object.defineProperty(toggleObj, '__key', {
      configurable: false,
      enumerable: false,
      writable: false,
    });
    // let's see if we can keep a ref to the parent (target)
    // this may enable key retrieval later
    Object.defineProperty(toggleObj, '__target', {
      configurable: false,
      enumerable: false,
      writable: false,
      value: target,
    });

    //  place a boolean key into the set when appropriate
    if (
      Boolean(memento) &&
      (equals(0, config.max) || gt(config.max, config.oSet.size))
    )
      config.oSet.add(toggleObj.__key);

    function replace(): boolean {
      const { value: currentKey } = config.oSet.keys().next();
      // the safer way is to check first
      if (!equals(currentKey, toggleObj.__key)) {
        currentKey && config.oSet.delete(currentKey);
        config.oSet.add(toggleObj.__key);
      }
      return true;
    }

    function range(force?: boolean): boolean {
      const current = toggleObj.isToggled;
      const b = equals(undefined, force) ? !current : Boolean(force);
      if (equals(current, b)) return current; // no change on a force attempt
      const sz = config.oSet.size;
      // adding
      if (b) {
        if (gte(sz, config.max)) return current; // no room at the inn
        config.oSet.add(toggleObj.__key);
        return true;
      }
      // deleting
      if (gte(config.min, sz)) return current; // no can do

      config.oSet.delete(toggleObj.__key);
      return false;
    }

    target[udpName] = toggleObj;
    return target;
  };

  decorate = Object.defineProperties(decorate, {
    max: {
      configurable: false,
      enumerable: true,
      get: function (): number {
        return config.max;
      },
      set: function (n: unknown): void {
        validateMax(n, config.min);
        // if it passes validation it's a number
        config.max = n as number;
      },
    },
    min: {
      configurable: false,
      enumerable: true,
      get: function (): number {
        return config.min;
      },
      set: function (n: unknown): void {
        validateMin(n, config.max);
        // if it passes validation it's a number
        config.min = n as number;
      },
    },
    size: {
      configurable: false,
      enumerable: true,
      writeable: false,
      get: function (): number {
        return config.oSet.size;
      },
    },
  });

  return decorate;
}
