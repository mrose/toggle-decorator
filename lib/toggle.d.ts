// Type definitions for toggleDecorator
// Project: https://github.com/mrose/toggleDecorator

// n.b. arbitrary properties may exist and are considered legal
export interface ToggleFactoryOptions {
  defaultValue: boolean;
  max: number;
  min: number;
  name: string;
  oSet: Set<unknown>;
  [key: string | symbol]: unknown;
}
declare type AllOptional<Type> = {
  [Property in keyof Type]+?: Type[Property];
};
export declare type ConfigOptions = AllOptional<ToggleFactoryOptions>;
export declare type ConfigArrArgs = [
  string?,
  number?,
  number?,
  boolean?,
  Set<unknown>?
];
export declare type ConfigOptionsArgs = [ConfigOptions?];

// all we know about Target is that it has an object shape
export interface Target {
  [key: string | symbol]: unknown;
}

export interface ToggleObj {
  readonly __key: symbol;
  readonly isToggled: boolean;
  readonly max: number;
  readonly memento: unknown;
  readonly min: number;
  readonly size: number;
  toggle: (force?: boolean) => boolean;
}

export type fnTargetDecorator = {
  (target: Target): Target;
  max: number;
  min: number;
  size: number;
};

export declare function toggle(
  ...args: ConfigArrArgs | ConfigOptionsArgs
): fnTargetDecorator;
