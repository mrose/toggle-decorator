import {
  ConfigArrArgs,
  ConfigOptions,
  ConfigOptionsArgs,
  ToggleFactoryOptions,
} from './toggle.d';
import {
  isPlainObject,
  validateDefaultValue,
  validateMax,
  validateMin,
  validateName,
} from './validators';

export function createToggleFactoryConfig(
  ...args: ConfigArrArgs | ConfigOptionsArgs
): ToggleFactoryOptions {
  const [nameOrOpts, max, min, defaultValue, oSet] = args;
  const opts = isPlainObject(nameOrOpts) ? nameOrOpts : {};
  const nm = isPlainObject(nameOrOpts) ? 'selected' : nameOrOpts;
  let config = {
    defaultValue: defaultValue ?? false,
    max: max ?? 1,
    min: min ?? 0,
    name: (nm as string) ?? 'selected',
    oSet: oSet ?? new Set(),
  };

  config = Object.assign(config, opts as ConfigOptions);

  validateDefaultValue(config.defaultValue);
  validateName(config.name);
  validateMax(config.max, config.min);
  validateMin(config.min, config.max);

  return config;
}
