import equals from 'ramda/src/equals';
import gt from 'ramda/src/gt';
import gte from 'ramda/src/gte';
import is from 'ramda/src/is';
import isNil from 'ramda/src/isNil';
import length from 'ramda/src/length';

export function isPlainObject(obj: unknown): boolean {
  if (typeof obj !== 'object' || obj === null) return false;
  if (isNil(obj)) return false;
  return Object.getPrototypeOf(obj) === Object.prototype;
}

export function validateDefaultValue(defaultValue: unknown): void {
  if (!is(Boolean, defaultValue))
    throw new Error(`'defaultValuel' must be boolean`);
}

export function validateMax(max: unknown, min: unknown): void {
  if (
    !is(Number, max) ||
    !Number.isFinite(max) ||
    !Number.isInteger(max) ||
    !gte(max, 0)
  )
    throw new Error(`'max' must be zero or a positive integer`);
  if (
    !is(Number, min) ||
    !Number.isFinite(min) ||
    !Number.isInteger(min) ||
    !gte(min, 0)
  )
    throw new Error(`'min' must be zero or a positive integer`);
  if (!gte(max, min))
    throw new Error(`'max' must be greater or equal to 'min'`);
  if (equals(max, min) && gt(max, 1))
    throw new Error(`'max' must be zero or one when 'min' and 'max' are equal`);
  if (gt(max, Number.MAX_SAFE_INTEGER))
    throw new Error(`max must be ${Number.MAX_SAFE_INTEGER} or lower`);
}

export function validateMin(min: unknown, max: unknown): void {
  if (
    !is(Number, min) ||
    !Number.isFinite(min) ||
    !Number.isInteger(min) ||
    !gte(min, 0)
  )
    throw new Error(`'min' must be zero or a positive integer`);
  if (
    !is(Number, max) ||
    !Number.isFinite(max) ||
    !Number.isInteger(max) ||
    !gte(max, 0)
  )
    throw new Error(`'max' must be zero or a positive integer`);
  if (!gte(max, min))
    throw new Error(`'min' must be less than or equal to 'max'`);
  if (equals(max, min) && gt(min, 1))
    throw new Error(`'min' must be zero or one when 'max' and 'min' are equal`);
}

export function validateName(name: unknown): void {
  if (!is(String, name)) throw new Error(`'name' must be a string`);
  if (equals(0, length(name))) throw new Error(`'name' cannot be blank`);
}

export function validateTarget(target: object): void {
  if (!isPlainObject(target)) throw new Error(`'target' must be an object`);
}
