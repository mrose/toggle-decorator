import map from 'ramda/src/map';
import { describe, expect, test } from 'vitest';

import { toggle } from '../toggle';

describe(`The decorate function returned from toggle()`, () => {
  test(`throws an exception when the 'target' argument is not an object`, () => {
    const decorate = toggle(); //use defaults
    // TODO: clients of this library may try to do stuff like this and
    // we need to assure that we have proper validation behaviour
    //expect(() => decorate(123)).toThrow();
    expect(() => decorate(['foo', 'bar', 'baz'])).toThrow();
    expect(() => decorate({ color: 'black' })).not.toThrow();
  });

  test(`has a max property`, () => {
    const decoratorFn = toggle();
    expect(decoratorFn).toHaveProperty('max');
    // the default is 1
    expect(decoratorFn.max).toBe(1);
  });

  test(`has a max property setter`, () => {
    const decoratorFn = toggle();
    decoratorFn.max = 10;
    expect(decoratorFn.max).toBe(10);
    expect(() => (decoratorFn.max = 'abc')).toThrow();
  });

  test(`has a min property`, () => {
    const decoratorFn = toggle();
    expect(decoratorFn).toHaveProperty('min');
    // the default is 0
    expect(decoratorFn.min).toBe(0);
  });

  test(`has a min property setter`, () => {
    const decoratorFn = toggle();
    decoratorFn.max = 100;
    decoratorFn.min = 10;
    expect(decoratorFn.min).toBe(10);
    expect(() => (decoratorFn.min = 'abc')).toThrow();
  });

  test(`has a size property`, () => {
    const decoratorFn = toggle();
    expect(decoratorFn).toHaveProperty('size');
    expect(decoratorFn.size).toBe(0);
  });

  test.todo(`todo: has a keys function`);

  test(`returns an object when the 'target' argument is an object`, () => {
    // use defaults
    const actual = toggle()({ id: 'red', selected: true });
    expect(actual).toBeInstanceOf(Object);
  });

  test(`places a '__toggle...' property on a 'target' object`, () => {
    let actual;

    // n.b. the first argument to the toggle function is used to calculate
    // the name of the property
    actual = toggle('selected')({ color: 'red' });
    expect(actual.__selectedToggle).toBeInstanceOf(Object);

    // you may need more than one
    actual = toggle('expanded')(actual);
    expect(actual.__expandedToggle).toBeInstanceOf(Object);
  });

  test(`places a non-enumerated toggle key with a unique value on the __toggle... property`, () => {
    const actual = toggle()({ color: 'red' });
    const tkd = Object.getOwnPropertyDescriptor(
      actual.__selectedToggle,
      '__key'
    );
    expect(typeof tkd.value).toBe('symbol');
  });

  test(`places a 'memento' property on the __toggle... property`, () => {
    const actual1 = toggle({ name: 'selected', defaultValue: false })({
      color: 'red',
    });
    expect(actual1.__selectedToggle.memento).toBe(false);

    const actual2 = toggle('selected')({ color: 'red', selected: false });
    expect(actual2.__selectedToggle.memento).toBe(false);

    const actual3 = toggle('checked')({ color: 'red', checked: true });
    expect(actual3.__checkedToggle.memento).toBe(true);

    const actual4 = toggle('expanded')({ color: 'red', expanded: 'truthy' });
    expect(actual4.__expandedToggle.memento).toBe('truthy');

    // you can't change the memento.
    expect(() => {
      actual4.__expandedToggle.memento = 'foo';
    }).toThrow();
  });

  test(`places an 'isToggled' property on the __toggle... object`, () => {
    const actual = toggle()({ color: 'red' });
    expect(actual.__selectedToggle).toHaveProperty('isToggled');
    // default is false
    expect(actual.__selectedToggle.isToggled).toBeFalsy();
  });

  // adds up to the max when max is greater than zero
  test(`the initial value of isToggled will be the boolean value of the 'name' configuration if it exists on the target object, until the max has been met`, () => {
    const targets = [
      { id: 'red', selected: true },
      { id: 'orange', selected: false },
      { id: 'yellow', selected: true },
      { id: 'green', selected: false },
      { id: 'blue', selected: true },
      { id: 'indigo', selected: false },
      { id: 'violet', selected: 'truthy' },
    ];
    const [red, orange, yellow, green, blue, indigo, violet] = map(
      toggle({ max: 3 }),
      targets
    );
    expect(red.__selectedToggle.isToggled).toBe(true);
    expect(orange.__selectedToggle.isToggled).toBe(false);
    expect(yellow.__selectedToggle.isToggled).toBe(true);
    expect(green.__selectedToggle.isToggled).toBe(false);
    expect(blue.__selectedToggle.isToggled).toBe(true);
    expect(indigo.__selectedToggle.isToggled).toBe(false);
    // hit the max. uh oh
    expect(violet.__selectedToggle.isToggled).toBe(false);
  });

  // adds all truthys when max is zero
  test(`when max is zero, the initial 'value' of isToggled will be the boolean value of the 'name' configuration on the target if it exists, or the defaultValue`, () => {
    const targets = [
      { id: 'red', selected: true },
      { id: 'orange', selected: false },
      { id: 'yellow', selected: true },
      { id: 'green', selected: false },
      { id: 'blue', selected: true },
      { id: 'indigo', selected: false },
      { id: 'violet', selected: 'truthy' },
    ];
    const [red, orange, yellow, green, blue, indigo, violet] = map(
      toggle('selected', 0),
      targets
    );
    expect(red.__selectedToggle.isToggled).toBe(true);
    expect(orange.__selectedToggle.isToggled).toBe(false);
    expect(yellow.__selectedToggle.isToggled).toBe(true);
    expect(green.__selectedToggle.isToggled).toBe(false);
    expect(blue.__selectedToggle.isToggled).toBe(true);
    expect(indigo.__selectedToggle.isToggled).toBe(false);
    expect(violet.__selectedToggle.isToggled).toBe(true);
  });

  test(`places a 'size' property on the __toggle... object`, () => {
    const targets = [
      { id: 'red', selected: true },
      { id: 'orange', selected: false },
      { id: 'yellow', selected: true },
      { id: 'green', selected: false },
      { id: 'blue', selected: true },
      { id: 'indigo', selected: false },
      { id: 'violet', selected: 'truthy' },
    ];
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [red, orange, yellow, green, blue, indigo, violet] = map(
      toggle('selected', 5),
      targets
    );
    expect(red.__selectedToggle.size).toBe(4);
  });

  test(`places a 'max' property on the __toggle... object`, () => {
    const actual = toggle({ max: 20, min: 10 })({
      color: 'red',
      selected: 'false',
    });
    expect(actual.__selectedToggle.max).toBe(20);
  });

  test(`places a 'min' property on the __toggle... object`, () => {
    const actual = toggle({ max: 20, min: 10 })({
      color: 'red',
      selected: 'false',
    });
    expect(actual.__selectedToggle.min).toBe(10);
  });

  test(`places a 'toggle' function on the __toggle... object`, () => {
    const actual = toggle()({ color: 'red', selected: 'false' });
    expect(actual.__selectedToggle.toggle).toBeInstanceOf(Function);
  });

  describe(`The toggle function`, () => {
    // noop
    describe(`when max is zero`, () => {
      test(`returns the current value (does not toggle)`, () => {
        const targets = [
          { id: 'red', selected: true },
          { id: 'orange', selected: true },
          { id: 'yellow', selected: true },
          { id: 'green' },
          { id: 'blue' },
          { id: 'indigo' },
          { id: 'violet' },
        ];
        // eslint-disable-next-line no-unused-vars
        const [red, orange, yellow, green, blue, indigo, violet] = map(
          toggle('selected', 0, 0),
          targets
        );
        expect(red.__selectedToggle.isToggled).toBe(true);
        expect(orange.__selectedToggle.isToggled).toBe(true);
        expect(yellow.__selectedToggle.isToggled).toBe(true);
        expect(green.__selectedToggle.isToggled).toBe(false);
        expect(blue.__selectedToggle.isToggled).toBe(false);
        expect(indigo.__selectedToggle.isToggled).toBe(false);
        expect(violet.__selectedToggle.isToggled).toBe(false);

        expect(red.__selectedToggle.toggle()).toBe(true);
        expect(orange.__selectedToggle.toggle()).toBe(true);
        expect(yellow.__selectedToggle.toggle()).toBe(true);
        expect(green.__selectedToggle.toggle()).toBe(false);
        expect(blue.__selectedToggle.toggle()).toBe(false);
        expect(indigo.__selectedToggle.toggle()).toBe(false);
        expect(violet.__selectedToggle.toggle()).toBe(false);
      });
    });

    // max 1 & min 1 = replace behaviour (typical toggling)
    describe(`when max is 1 and min is 1`, () => {
      describe(`when the target is already toggled true`, () => {
        test(`return true (do not toggle)`, () => {
          const targets = [
            { id: 'red', selected: true },
            { id: 'orange' },
            { id: 'yellow' },
            { id: 'green' },
            { id: 'blue' },
            { id: 'indigo' },
            { id: 'violet' },
          ];
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const [red, orange, yellow, green, blue, indigo, violet] = map(
            toggle('selected', 1, 1),
            targets
          );
          expect(red.__selectedToggle.isToggled).toBe(true);
          // ok now toggle
          expect(red.__selectedToggle.toggle()).toBe(true);

          expect(red.__selectedToggle.isToggled).toBe(true);
        });
      });
      describe(`when the target is not toggled true`, () => {
        test(`return true (untoggle the current target then toggle true)`, () => {
          const targets = [
            { id: 'red', selected: true },
            { id: 'orange' },
            { id: 'yellow' },
            { id: 'green' },
            { id: 'blue' },
            { id: 'indigo' },
            { id: 'violet' },
          ];
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const [red, orange, yellow, green, blue, indigo, violet] = map(
            toggle('selected', 1, 1),
            targets
          );
          expect(red.__selectedToggle.isToggled).toBe(true);
          expect(orange.__selectedToggle.isToggled).toBe(false);
          // ok now toggle
          expect(orange.__selectedToggle.toggle()).toBe(true);

          expect(orange.__selectedToggle.isToggled).toBe(true);
          expect(red.__selectedToggle.isToggled).toBe(false);
        });
      });
    });

    // range behaviour
    describe(`when max more than zero and min is less than max`, () => {
      // adding
      describe(`when the value is true`, () => {
        describe(`when the number of toggled true items is greater or equal to max`, () => {
          test(`when the item is not toggled, do nothing and return false`, () => {
            const targets = [
              { id: 'red', selected: true },
              { id: 'orange', selected: true },
              { id: 'yellow', selected: true },
              { id: 'green' },
              { id: 'blue' },
              { id: 'indigo' },
              { id: 'violet' },
            ];
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const [red, orange, yellow, green, blue, indigo, violet] = map(
              toggle('selected', 3, 0),
              targets
            );
            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(true);
            expect(green.__selectedToggle.isToggled).toBe(false);
            // ok now toggle
            expect(green.__selectedToggle.toggle()).toBe(false);

            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(true);
            expect(green.__selectedToggle.isToggled).toBe(false);
          });
          test(`when the item is toggled true, do nothing and return true`, () => {
            const targets = [
              { id: 'red', selected: true },
              { id: 'orange', selected: true },
              { id: 'yellow', selected: true },
              { id: 'green' },
              { id: 'blue' },
              { id: 'indigo' },
              { id: 'violet' },
            ];
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const [red, orange, yellow, green, blue, indigo, violet] = map(
              toggle('selected', 3, 0),
              targets
            );
            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(true);
            expect(green.__selectedToggle.isToggled).toBe(false);

            // force toggle
            expect(red.__selectedToggle.toggle(true)).toBe(true);

            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(true);
            expect(green.__selectedToggle.isToggled).toBe(false);
          });
        });
        describe(`when the number of toggled true items is lower than max`, () => {
          test(`when the item is not toggled, toggle true and return true`, () => {
            const targets = [
              { id: 'red', selected: true },
              { id: 'orange', selected: true },
              { id: 'yellow' },
              { id: 'green' },
              { id: 'blue' },
              { id: 'indigo' },
              { id: 'violet' },
            ];
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const [red, orange, yellow, green, blue, indigo, violet] = map(
              toggle('selected', 3, 0),
              targets
            );
            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(false);

            // ok now toggle
            expect(yellow.__selectedToggle.toggle()).toBe(true);

            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(true);
          });
          test(`when the item is toggled true, do nothing and return true`, () => {
            const targets = [
              { id: 'red', selected: true },
              { id: 'orange', selected: true },
              { id: 'yellow' },
              { id: 'green' },
              { id: 'blue' },
              { id: 'indigo' },
              { id: 'violet' },
            ];
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const [red, orange, yellow, green, blue, indigo, violet] = map(
              toggle('selected', 3, 0),
              targets
            );
            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(false);

            // force toggle
            expect(orange.__selectedToggle.toggle(true)).toBe(true);

            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(false);
          });
        });
      });
      // removing
      describe(`when the value is false`, () => {
        describe(`when the number of toggled true items is lower or equal to min`, () => {
          test(`when the item is not toggled, do nothing and return false`, () => {
            const targets = [
              { id: 'red', selected: true },
              { id: 'orange', selected: true },
              { id: 'yellow' },
              { id: 'green' },
              { id: 'blue' },
              { id: 'indigo' },
              { id: 'violet' },
            ];
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const [red, orange, yellow, green, blue, indigo, violet] = map(
              toggle('selected', 5, 2),
              targets
            );
            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(false);

            // force toggle false
            expect(yellow.__selectedToggle.toggle(false)).toBe(false);

            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(false);
          });
          test(`when the item is toggled true, do nothing and return true`, () => {
            const targets = [
              { id: 'red', selected: true },
              { id: 'orange', selected: true },
              { id: 'yellow' },
              { id: 'green' },
              { id: 'blue' },
              { id: 'indigo' },
              { id: 'violet' },
            ];
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const [red, orange, yellow, green, blue, indigo, violet] = map(
              toggle('selected', 5, 2),
              targets
            );
            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(false);

            // force toggle
            expect(orange.__selectedToggle.toggle(false)).toBe(true);

            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(false);
          });
        });
        describe(`when the number of toggled true items is higher than min`, () => {
          test(`when the item is not toggled, do nothing and return false`, () => {
            const targets = [
              { id: 'red', selected: true },
              { id: 'orange', selected: true },
              { id: 'yellow', selected: true },
              { id: 'green' },
              { id: 'blue' },
              { id: 'indigo' },
              { id: 'violet' },
            ];
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const [red, orange, yellow, green, blue, indigo, violet] = map(
              toggle('selected', 5, 2),
              targets
            );
            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(true);
            expect(green.__selectedToggle.isToggled).toBe(false);

            // force toggle
            expect(green.__selectedToggle.toggle(false)).toBe(false);

            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(true);
            expect(green.__selectedToggle.isToggled).toBe(false);
          });
          test(`when the item is toggled true, toggle false and return false`, () => {
            const targets = [
              { id: 'red', selected: true },
              { id: 'orange', selected: true },
              { id: 'yellow', selected: true },
              { id: 'green', selected: true },
              { id: 'blue' },
              { id: 'indigo' },
              { id: 'violet' },
            ];
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const [red, orange, yellow, green, blue, indigo, violet] = map(
              toggle('selected', 5, 2),
              targets
            );
            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(true);
            expect(green.__selectedToggle.isToggled).toBe(true);

            // regular toggle
            expect(green.__selectedToggle.toggle()).toBe(false);

            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(true);
            expect(green.__selectedToggle.isToggled).toBe(false);

            // toggle back to true
            expect(green.__selectedToggle.toggle()).toBe(true);

            // force toggle should also return false
            expect(green.__selectedToggle.toggle(false)).toBe(false);

            expect(red.__selectedToggle.isToggled).toBe(true);
            expect(orange.__selectedToggle.isToggled).toBe(true);
            expect(yellow.__selectedToggle.isToggled).toBe(true);
            expect(green.__selectedToggle.isToggled).toBe(false);
          });
        });
      });
    });
  });

  test(`The size property returns the number of true targets`, () => {
    const targets = [
      { id: 'red', selected: true },
      { id: 'orange', selected: false },
      { id: 'yellow', selected: true },
      { id: 'green', selected: false },
      { id: 'blue', selected: true },
      { id: 'indigo', selected: false },
      { id: 'violet', selected: 'truthy' },
    ];
    // eslint-disable-next-line no-unused-vars
    const [red, orange, yellow, green, blue, indigo, violet] = map(
      toggle('selected', 10),
      targets
    );
    expect(red.__selectedToggle.size).toBe(4);
    expect(orange.__selectedToggle.size).toBe(4);
    expect(yellow.__selectedToggle.size).toBe(4);
    expect(green.__selectedToggle.size).toBe(4);
    expect(blue.__selectedToggle.size).toBe(4);
    expect(indigo.__selectedToggle.size).toBe(4);
    expect(violet.__selectedToggle.size).toBe(4);

    indigo.__selectedToggle.toggle();

    expect(red.__selectedToggle.size).toBe(5);
    expect(orange.__selectedToggle.size).toBe(5);
    expect(yellow.__selectedToggle.size).toBe(5);
    expect(green.__selectedToggle.size).toBe(5);
    expect(blue.__selectedToggle.size).toBe(5);
    expect(indigo.__selectedToggle.size).toBe(5);
    expect(violet.__selectedToggle.size).toBe(5);
  });
});
