import { describe, expect, test } from 'vitest';

import { createToggleFactoryConfig } from '../configure';

describe(`The createToggleFactoryConfig function`, () => {
  test(`should be a function`, () => {
    expect(createToggleFactoryConfig).toBeInstanceOf(Function);
  });

  // toggle uses createToggleFactoryConfig internally but we test it here for convenience
  describe(`when provided zero arguments`, () => {
    test(`returns the default configuration object`, () => {
      const actual = createToggleFactoryConfig();
      expect(actual.name).toBe('selected');
      expect(actual.max).toBe(1);
      expect(actual.min).toBe(0);
      expect(actual.defaultValue).toBe(false);
    });
  });
  describe(`when provided a string for the toggle name`, () => {
    test(`throws when provided a zero length string`, () => {
      expect(() => createToggleFactoryConfig('')).toThrow();
    });
    test(`throws when provided a non-string value`, () => {
      expect(() => createToggleFactoryConfig(123)).toThrow();
    });
    test(`validates the name and returns a configuration object`, () => {
      const actual = createToggleFactoryConfig('checked');
      expect(actual.name).toBe('checked');
      expect(actual.max).toBe(1);
      expect(actual.min).toBe(0);
      expect(actual.defaultValue).toBe(false);
    });
  });
  describe(`when provided an object with some configuration`, () => {
    describe(`validates the provided configuration`, () => {
      test(`throws when provided a zero length string`, () => {
        expect(() => createToggleFactoryConfig({ name: '' })).toThrow();
      });
      test(`throws when provided a non-string value`, () => {
        expect(() => createToggleFactoryConfig({ name: 123 })).toThrow();
      });
      test(`throws when 'max' is not a non-negative integer`, () => {
        expect(() => createToggleFactoryConfig({ max: -1 })).toThrow();
        expect(() => createToggleFactoryConfig({ max: 'abc' })).toThrow();
      });
      test(`throws when 'min'  is not a non-negative integer`, () => {
        expect(() => createToggleFactoryConfig({ min: -1 })).toThrow();
        expect(() => createToggleFactoryConfig({ min: 'abc' })).toThrow();
      });
      test(`throws when 'max' is not greater or equal to 'min'`, () => {
        expect(() => createToggleFactoryConfig({ max: 1, min: 2 })).toThrow();
      });
      test(`throws when 'max' and 'min' are both equal to any number greater than 1`, () => {
        expect(() => createToggleFactoryConfig({ max: 2, min: 2 })).toThrow();
      });
      test(`throws when 'defaultValue' argument is not a boolean`, () => {
        expect(() =>
          createToggleFactoryConfig({ defaultValue: 'foo' })
        ).toThrow();
      });
    });
    test(`returns a valid configuration object`, () => {
      const actual = createToggleFactoryConfig({
        name: 'checked',
        max: 10,
        min: 1,
        defaultValue: true,
      });

      expect(actual.name).toBe('checked');
      expect(actual.max).toBe(10);
      expect(actual.min).toBe(1);
      expect(actual.defaultValue).toBe(true);
    });
  });
  describe(`when provided arguments in the expected order`, () => {
    describe(`validates the provided configuration`, () => {
      test(`throws when provided a zero length string`, () => {
        expect(() => createToggleFactoryConfig('')).toThrow();
      });
      test(`throws when provided a non-string value`, () => {
        expect(() => createToggleFactoryConfig(123)).toThrow();
      });
      test(`throws when 'max' is not a non-negative integer`, () => {
        expect(() => createToggleFactoryConfig('checked', -1)).toThrow();
        expect(() => createToggleFactoryConfig('checked', 'abc')).toThrow();
      });
      test(`throws when 'min'  is not a non-negative integer`, () => {
        expect(() => createToggleFactoryConfig({ min: -1 })).toThrow();
        expect(() => createToggleFactoryConfig({ min: 'abc' })).toThrow();
      });
      test(`throws when 'max' is not greater or equal to 'min'`, () => {
        expect(() => createToggleFactoryConfig({ max: 1, min: 2 })).toThrow();
      });
      test(`throws when 'defaultValue' argument is not a boolean`, () => {
        expect(() =>
          createToggleFactoryConfig({ defaultValue: 'foo' })
        ).toThrow();
      });
    });
    test(`returns a valid configuration object`, () => {
      const actual = createToggleFactoryConfig('checked', 10, 1, true);
      expect(actual.name).toBe('checked');
      expect(actual.max).toBe(10);
      expect(actual.min).toBe(1);
      expect(actual.defaultValue).toBe(true);
    });
    test(`allows arbitrary configuration properties to be provided by using a configuration object`, () => {
      const actual = createToggleFactoryConfig({
        name: 'checked',
        max: 10,
        min: 1,
        defaultValue: true,
        foo: 'bar',
        baz: 'qux',
      });
      expect(actual.foo).toBe('bar');
      expect(actual.baz).toBe('qux');
    });
  });
});
