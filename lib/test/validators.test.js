import { describe, expect, test } from 'vitest';

import {
  validateDefaultValue,
  validateMax,
  validateMin,
  validateName,
  validateTarget,
} from '../validators';

describe(`The validateDefaultValue function`, () => {
  test(`should be a function`, () => {
    expect(validateDefaultValue).toBeInstanceOf(Function);
  });

  test(`should accept boolean values`, () => {
    expect(() => validateDefaultValue(true)).not.toThrow();
    expect(() => validateDefaultValue(false)).not.toThrow();
  });

  test(`should throw when provided a string`, () => {
    expect(() => validateDefaultValue('abc')).toThrow();
  });
});

describe(`The validateMax function`, () => {
  test(`should be a function`, () => {
    expect(validateMax).toBeInstanceOf(Function);
  });

  test(`should throw when provided a negative number for max`, () => {
    expect(() => validateMax(-1, 0)).toThrow();
  });

  test(`should throw when provided a negative number for min`, () => {
    expect(() => validateMax(0, -1)).toThrow();
  });

  test(`should throw when min is greater than max`, () => {
    expect(() => validateMax(1, 2)).toThrow();
  });

  test(`should throw when min and max are equal and both more than one`, () => {
    expect(() => validateMax(2, 2)).toThrow();
  });
});

describe(`The validateMin function`, () => {
  test(`should be a function`, () => {
    expect(validateMin).toBeInstanceOf(Function);
  });

  test(`should throw when provided a negative number for min`, () => {
    expect(() => validateMin(0, -1)).toThrow();
  });

  test(`should throw when provided a negative number for max`, () => {
    expect(() => validateMin(-1, 0)).toThrow();
  });

  test(`should throw when min is greater than max`, () => {
    expect(() => validateMin(2, 1)).toThrow();
  });

  test(`should throw when min and max are equal and both more than one`, () => {
    expect(() => validateMax(2, 2)).toThrow();
  });
});

describe(`The validateName function`, () => {
  test(`should be a function`, () => {
    expect(validateName).toBeInstanceOf(Function);
  });

  test(`should throw when not provided a string`, () => {
    expect(() => validateName(123)).toThrow();
  });

  test(`should throw when the string provided has no length`, () => {
    expect(() => validateName('')).toThrow();
  });
});

describe(`The validateTarget function`, () => {
  test(`should be a function`, () => {
    expect(validateTarget).toBeInstanceOf(Function);
  });

  test(`should throw when not provided a real object`, () => {
    expect(() => validateTarget('')).toThrow();
    expect(() => validateTarget([])).toThrow();
    const ta = new Float32Array();
    expect(() => validateTarget(ta)).toThrow();
    expect(() => validateTarget({})).not.toThrow();
  });
});
