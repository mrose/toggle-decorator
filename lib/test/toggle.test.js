import { describe, expect, test } from 'vitest';

import { toggle } from '../toggle';

describe(`toggle()`, () => {
  test(`should be a function`, () => {
    expect(toggle).toBeInstanceOf(Function);
  });

  test(`should return a toggling decorator function`, () => {
    expect(toggle()).toBeInstanceOf(Function);
  });
});
