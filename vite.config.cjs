/// <reference types="vite/client" />
/// <reference types="vitest" />

const path = require('path');
const { defineConfig } = require('vite');
const { visualizer } = require('rollup-plugin-visualizer');

// https://vitejs.dev/config/
module.exports = defineConfig({
  build: {
    lib: {
      entry: path.resolve(__dirname, 'lib/index.ts'),
      fileName: (format) => `toggle.${format}.js`,
      name: 'toggle',
    },
    rollupOptions: {
      // https://rollupjs.org/guide/en/#big-list-of-options
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: ['react', 'react-dom'],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        // globals: { vue: 'Vue', },
      },
    },
  },
  // will generate stats.html
  // plugins: [visualizer()],
  test: {
    global: true,
    environment: 'jsdom',
  },
});
