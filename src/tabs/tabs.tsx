import './tabs.css';

import React, { Children, useState } from 'react';
import { Target, toggle, ToggleObj } from 'toggle-decorator';

// n.b. lots of holes in this code. don't use it in production
// too much bother to make a compound component example for now

interface iTab extends Target {
  __selectedToggle: ToggleObj;
  children: JSX.Element | JSX.Element[] | string;
  label: string;
}

interface iTabProps extends Record<string, unknown> {
  children: JSX.Element | JSX.Element[] | string;
  label: string;
}

interface iTabSetProps {
  children: JSX.Element | JSX.Element[];
}

const TabSet = ({ children }: iTabSetProps) => {
  const td = toggle('selected', 1, 1);
  // we are not checking labels for uniqueness but should
  const initialState = Children.map(children, (child) => ({
    children: child?.props?.children,
    label: child?.props?.label ?? 'label missing',
  })).map(td) as iTab[];
  // default the first
  initialState[0].__selectedToggle.toggle();
  const [tabs, setTabs] = useState(initialState);

  const handleTabClick = (tab: iTab) => () => {
    tab.__selectedToggle.toggle();
    setTabs([...tabs]); // cheat: bump state to rerender
  };

  return (
    <div className="tab-set">
      <ol>
        {tabs.map((tab) => {
          const active = tab.__selectedToggle.isToggled ? 'active' : '';
          return (
            <li
              key={tab.label}
              className={`${active}`}
              onClick={handleTabClick(tab)}
            >
              {tab.label}
            </li>
          );
        })}
      </ol>
      <div className="tab-content">
        {tabs.map((tab) => {
          return tab.__selectedToggle.isToggled ? tab.children : undefined;
        })}
      </div>
    </div>
  );
};

// this dangerous comment will never appear in the DOM :)
const Tab = ({ label }: iTabProps) => (
  <span
    dangerouslySetInnerHTML={{
      __html: `<!-- ${label} -->`,
    }}
  />
);

export { Tab, TabSet };
