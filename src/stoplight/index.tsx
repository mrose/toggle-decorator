import './index.css';

import React, { useState } from 'react';
import { Target, toggle, ToggleObj } from 'toggle-decorator';

interface Bulb extends Target {
  __onToggle: ToggleObj;
  color: 'red' | 'yellow' | 'green';
  next: 'red' | 'yellow' | 'green';
}

interface StopLight extends Record<string, Bulb> {
  red: Bulb;
  yellow: Bulb;
  green: Bulb;
}

interface BulbProps extends Record<string, unknown> {
  light: Bulb;
}

const Bulb = ({ light }: BulbProps) => {
  const clr = light.__onToggle.isToggled ? light.color : '';
  return <div className={`circle ${clr}`} />;
};

const StopLight = () => {
  const [red, yellow, green] = [
    { color: 'red', next: 'yellow', on: true },
    { color: 'yellow', next: 'green', on: false },
    { color: 'green', next: 'red', on: false },
  ].map(toggle('on', 1, 1));
  const [lights, setLights] = useState({ red, yellow, green } as StopLight);

  const handleButtonClick = () => {
    const cur = [lights.red, lights.yellow, lights.green].find(
      (bulb) => bulb.__onToggle.isToggled
    );
    const next = cur?.next ?? lights.green.next;
    lights[next].__onToggle.toggle();
    setLights({ ...lights });
  };

  return (
    <>
      <div className="stoplight">
        <Bulb light={lights['red']} />
        <Bulb light={lights['yellow']} />
        <Bulb light={lights['green']} />
      </div>
      <button className="stoplight-change-button" onClick={handleButtonClick}>
        Change
      </button>
    </>
  );
};

export { StopLight };
