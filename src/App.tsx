import './index.css';
import './App.css';
import './tabs/tabs.css';

import React from 'react';

import { StopLight } from './stoplight';
import { Tab, TabSet } from './tabs/tabs';

export function App() {
  return (
    <>
      <h2>Examples</h2>
      <TabSet>
        <Tab label="Stoplight">
          <StopLight />
        </Tab>
        <Tab label="Example 2">Hello</Tab>
        <Tab label="Example 3">World</Tab>
        <Tab label="Example 4">Goodbye</Tab>
      </TabSet>
    </>
  );
}
