function _isPlaceholder$2(a) {
  return a != null && typeof a === "object" && a["@@functional/placeholder"] === true;
}
var _isPlaceholder_1 = _isPlaceholder$2;
var _isPlaceholder$1 = _isPlaceholder_1;
function _curry1$5(fn) {
  return function f1(a) {
    if (arguments.length === 0 || _isPlaceholder$1(a)) {
      return f1;
    } else {
      return fn.apply(this, arguments);
    }
  };
}
var _curry1_1 = _curry1$5;
var _curry1$4 = _curry1_1;
var _isPlaceholder = _isPlaceholder_1;
function _curry2$6(fn) {
  return function f2(a, b) {
    switch (arguments.length) {
      case 0:
        return f2;
      case 1:
        return _isPlaceholder(a) ? f2 : _curry1$4(function(_b) {
          return fn(a, _b);
        });
      default:
        return _isPlaceholder(a) && _isPlaceholder(b) ? f2 : _isPlaceholder(a) ? _curry1$4(function(_a) {
          return fn(_a, b);
        }) : _isPlaceholder(b) ? _curry1$4(function(_b) {
          return fn(a, _b);
        }) : fn(a, b);
    }
  };
}
var _curry2_1 = _curry2$6;
function _arrayFromIterator$1(iter) {
  var list = [];
  var next;
  while (!(next = iter.next()).done) {
    list.push(next.value);
  }
  return list;
}
var _arrayFromIterator_1 = _arrayFromIterator$1;
function _includesWith$1(pred, x, list) {
  var idx = 0;
  var len = list.length;
  while (idx < len) {
    if (pred(x, list[idx])) {
      return true;
    }
    idx += 1;
  }
  return false;
}
var _includesWith_1 = _includesWith$1;
function _functionName$1(f) {
  var match = String(f).match(/^function (\w*)/);
  return match == null ? "" : match[1];
}
var _functionName_1 = _functionName$1;
function _has$4(prop, obj) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}
var _has_1 = _has$4;
function _objectIs$1(a, b) {
  if (a === b) {
    return a !== 0 || 1 / a === 1 / b;
  } else {
    return a !== a && b !== b;
  }
}
var _objectIs_1 = typeof Object.is === "function" ? Object.is : _objectIs$1;
var _has$3 = _has_1;
var toString = Object.prototype.toString;
var _isArguments$1 = /* @__PURE__ */ function() {
  return toString.call(arguments) === "[object Arguments]" ? function _isArguments2(x) {
    return toString.call(x) === "[object Arguments]";
  } : function _isArguments2(x) {
    return _has$3("callee", x);
  };
}();
var _isArguments_1 = _isArguments$1;
var _curry1$3 = _curry1_1;
var _has$2 = _has_1;
var _isArguments = _isArguments_1;
var hasEnumBug = !/* @__PURE__ */ {
  toString: null
}.propertyIsEnumerable("toString");
var nonEnumerableProps = ["constructor", "valueOf", "isPrototypeOf", "toString", "propertyIsEnumerable", "hasOwnProperty", "toLocaleString"];
var hasArgsEnumBug = /* @__PURE__ */ function() {
  return arguments.propertyIsEnumerable("length");
}();
var contains = function contains2(list, item) {
  var idx = 0;
  while (idx < list.length) {
    if (list[idx] === item) {
      return true;
    }
    idx += 1;
  }
  return false;
};
var keys$1 = typeof Object.keys === "function" && !hasArgsEnumBug ? /* @__PURE__ */ _curry1$3(function keys(obj) {
  return Object(obj) !== obj ? [] : Object.keys(obj);
}) : /* @__PURE__ */ _curry1$3(function keys2(obj) {
  if (Object(obj) !== obj) {
    return [];
  }
  var prop, nIdx;
  var ks = [];
  var checkArgsLength = hasArgsEnumBug && _isArguments(obj);
  for (prop in obj) {
    if (_has$2(prop, obj) && (!checkArgsLength || prop !== "length")) {
      ks[ks.length] = prop;
    }
  }
  if (hasEnumBug) {
    nIdx = nonEnumerableProps.length - 1;
    while (nIdx >= 0) {
      prop = nonEnumerableProps[nIdx];
      if (_has$2(prop, obj) && !contains(ks, prop)) {
        ks[ks.length] = prop;
      }
      nIdx -= 1;
    }
  }
  return ks;
});
var keys_1 = keys$1;
var _curry1$2 = _curry1_1;
var type$1 = /* @__PURE__ */ _curry1$2(function type(val) {
  return val === null ? "Null" : val === void 0 ? "Undefined" : Object.prototype.toString.call(val).slice(8, -1);
});
var type_1 = type$1;
var _arrayFromIterator = _arrayFromIterator_1;
var _includesWith = _includesWith_1;
var _functionName = _functionName_1;
var _has$1 = _has_1;
var _objectIs = _objectIs_1;
var keys3 = keys_1;
var type2 = type_1;
function _uniqContentEquals(aIterator, bIterator, stackA, stackB) {
  var a = _arrayFromIterator(aIterator);
  var b = _arrayFromIterator(bIterator);
  function eq(_a, _b) {
    return _equals$1(_a, _b, stackA.slice(), stackB.slice());
  }
  return !_includesWith(function(b2, aItem) {
    return !_includesWith(eq, aItem, b2);
  }, b, a);
}
function _equals$1(a, b, stackA, stackB) {
  if (_objectIs(a, b)) {
    return true;
  }
  var typeA = type2(a);
  if (typeA !== type2(b)) {
    return false;
  }
  if (a == null || b == null) {
    return false;
  }
  if (typeof a["fantasy-land/equals"] === "function" || typeof b["fantasy-land/equals"] === "function") {
    return typeof a["fantasy-land/equals"] === "function" && a["fantasy-land/equals"](b) && typeof b["fantasy-land/equals"] === "function" && b["fantasy-land/equals"](a);
  }
  if (typeof a.equals === "function" || typeof b.equals === "function") {
    return typeof a.equals === "function" && a.equals(b) && typeof b.equals === "function" && b.equals(a);
  }
  switch (typeA) {
    case "Arguments":
    case "Array":
    case "Object":
      if (typeof a.constructor === "function" && _functionName(a.constructor) === "Promise") {
        return a === b;
      }
      break;
    case "Boolean":
    case "Number":
    case "String":
      if (!(typeof a === typeof b && _objectIs(a.valueOf(), b.valueOf()))) {
        return false;
      }
      break;
    case "Date":
      if (!_objectIs(a.valueOf(), b.valueOf())) {
        return false;
      }
      break;
    case "Error":
      return a.name === b.name && a.message === b.message;
    case "RegExp":
      if (!(a.source === b.source && a.global === b.global && a.ignoreCase === b.ignoreCase && a.multiline === b.multiline && a.sticky === b.sticky && a.unicode === b.unicode)) {
        return false;
      }
      break;
  }
  var idx = stackA.length - 1;
  while (idx >= 0) {
    if (stackA[idx] === a) {
      return stackB[idx] === b;
    }
    idx -= 1;
  }
  switch (typeA) {
    case "Map":
      if (a.size !== b.size) {
        return false;
      }
      return _uniqContentEquals(a.entries(), b.entries(), stackA.concat([a]), stackB.concat([b]));
    case "Set":
      if (a.size !== b.size) {
        return false;
      }
      return _uniqContentEquals(a.values(), b.values(), stackA.concat([a]), stackB.concat([b]));
    case "Arguments":
    case "Array":
    case "Object":
    case "Boolean":
    case "Number":
    case "String":
    case "Date":
    case "Error":
    case "RegExp":
    case "Int8Array":
    case "Uint8Array":
    case "Uint8ClampedArray":
    case "Int16Array":
    case "Uint16Array":
    case "Int32Array":
    case "Uint32Array":
    case "Float32Array":
    case "Float64Array":
    case "ArrayBuffer":
      break;
    default:
      return false;
  }
  var keysA = keys3(a);
  if (keysA.length !== keys3(b).length) {
    return false;
  }
  var extendedStackA = stackA.concat([a]);
  var extendedStackB = stackB.concat([b]);
  idx = keysA.length - 1;
  while (idx >= 0) {
    var key = keysA[idx];
    if (!(_has$1(key, b) && _equals$1(b[key], a[key], extendedStackA, extendedStackB))) {
      return false;
    }
    idx -= 1;
  }
  return true;
}
var _equals_1 = _equals$1;
var _curry2$5 = _curry2_1;
var _equals = _equals_1;
var equals = /* @__PURE__ */ _curry2$5(function equals2(a, b) {
  return _equals(a, b, [], []);
});
var equals_1 = equals;
var equals$1 = equals_1;
var _curry2$4 = _curry2_1;
var gt = /* @__PURE__ */ _curry2$4(function gt2(a, b) {
  return a > b;
});
var gt_1 = gt;
var gt$1 = gt_1;
var _curry2$3 = _curry2_1;
var gte = /* @__PURE__ */ _curry2$3(function gte2(a, b) {
  return a >= b;
});
var gte_1 = gte;
var gte$1 = gte_1;
var _curry1$1 = _curry1_1;
var isNil$1 = /* @__PURE__ */ _curry1$1(function isNil(x) {
  return x == null;
});
var isNil_1 = isNil$1;
var isNil$2 = isNil_1;
var _curry2$2 = _curry2_1;
var _has = _has_1;
var isNil2 = isNil_1;
var hasPath$1 = /* @__PURE__ */ _curry2$2(function hasPath(_path, obj) {
  if (_path.length === 0 || isNil2(obj)) {
    return false;
  }
  var val = obj;
  var idx = 0;
  while (idx < _path.length) {
    if (!isNil2(val) && _has(_path[idx], val)) {
      val = val[_path[idx]];
      idx += 1;
    } else {
      return false;
    }
  }
  return true;
});
var hasPath_1 = hasPath$1;
var _curry2$1 = _curry2_1;
var hasPath2 = hasPath_1;
var has = /* @__PURE__ */ _curry2$1(function has2(prop, obj) {
  return hasPath2([prop], obj);
});
var has_1 = has;
var has$1 = has_1;
var _curry2 = _curry2_1;
var is = /* @__PURE__ */ _curry2(function is2(Ctor, val) {
  return val != null && val.constructor === Ctor || val instanceof Ctor;
});
var is_1 = is;
var is$1 = is_1;
function _isNumber$1(x) {
  return Object.prototype.toString.call(x) === "[object Number]";
}
var _isNumber_1 = _isNumber$1;
var _curry1 = _curry1_1;
var _isNumber = _isNumber_1;
var length = /* @__PURE__ */ _curry1(function length2(list) {
  return list != null && _isNumber(list.length) ? list.length : NaN;
});
var length_1 = length;
var length$1 = length_1;
function isPlainObject(obj) {
  if (typeof obj !== "object" || obj === null)
    return false;
  if (isNil$2(obj))
    return false;
  return Object.getPrototypeOf(obj) === Object.prototype;
}
function validateDefaultValue(defaultValue) {
  if (!is$1(Boolean, defaultValue))
    throw new Error(`'defaultValuel' must be boolean`);
}
function validateMax(max, min) {
  if (!is$1(Number, max) || !Number.isFinite(max) || !Number.isInteger(max) || !gte$1(max, 0))
    throw new Error(`'max' must be zero or a positive integer`);
  if (!is$1(Number, min) || !Number.isFinite(min) || !Number.isInteger(min) || !gte$1(min, 0))
    throw new Error(`'min' must be zero or a positive integer`);
  if (!gte$1(max, min))
    throw new Error(`'max' must be greater or equal to 'min'`);
  if (equals$1(max, min) && gt$1(max, 1))
    throw new Error(`'max' must be zero or one when 'min' and 'max' are equal`);
  if (gt$1(max, Number.MAX_SAFE_INTEGER))
    throw new Error(`max must be ${Number.MAX_SAFE_INTEGER} or lower`);
}
function validateMin(min, max) {
  if (!is$1(Number, min) || !Number.isFinite(min) || !Number.isInteger(min) || !gte$1(min, 0))
    throw new Error(`'min' must be zero or a positive integer`);
  if (!is$1(Number, max) || !Number.isFinite(max) || !Number.isInteger(max) || !gte$1(max, 0))
    throw new Error(`'max' must be zero or a positive integer`);
  if (!gte$1(max, min))
    throw new Error(`'min' must be less than or equal to 'max'`);
  if (equals$1(max, min) && gt$1(min, 1))
    throw new Error(`'min' must be zero or one when 'max' and 'min' are equal`);
}
function validateName(name) {
  if (!is$1(String, name))
    throw new Error(`'name' must be a string`);
  if (equals$1(0, length$1(name)))
    throw new Error(`'name' cannot be blank`);
}
function validateTarget(target) {
  if (!isPlainObject(target))
    throw new Error(`'target' must be an object`);
}
function createToggleFactoryConfig(...args) {
  const [nameOrOpts, max, min, defaultValue, oSet] = args;
  const opts = isPlainObject(nameOrOpts) ? nameOrOpts : {};
  const nm = isPlainObject(nameOrOpts) ? "selected" : nameOrOpts;
  let config = {
    defaultValue: defaultValue != null ? defaultValue : false,
    max: max != null ? max : 1,
    min: min != null ? min : 0,
    name: nm != null ? nm : "selected",
    oSet: oSet != null ? oSet : new Set()
  };
  config = Object.assign(config, opts);
  validateDefaultValue(config.defaultValue);
  validateName(config.name);
  validateMax(config.max, config.min);
  validateMin(config.min, config.max);
  return config;
}
function toggle(...args) {
  const config = createToggleFactoryConfig(...args);
  let decorate = function decorate2(target) {
    validateTarget(target);
    const udpName = `__${config.name}Toggle`;
    const memento = has$1(config.name, target) ? target[config.name] : config.defaultValue;
    const toggleObj = {
      __key: Symbol("key"),
      get isToggled() {
        return config.oSet.has(toggleObj.__key);
      },
      get memento() {
        return memento;
      },
      set memento(m) {
        throw new Error("memento cannot be reset");
      },
      get max() {
        return config.max;
      },
      get min() {
        return config.min;
      },
      get size() {
        return config.oSet.size;
      },
      toggle: function(force) {
        if (equals$1(0, config.max))
          return toggleObj.isToggled;
        if (equals$1(1, config.max) && equals$1(1, config.min))
          return replace();
        return range(force);
      }
    };
    Object.defineProperty(toggleObj, "__key", {
      configurable: false,
      enumerable: false,
      writable: false
    });
    Object.defineProperty(toggleObj, "__target", {
      configurable: false,
      enumerable: false,
      writable: false,
      value: target
    });
    if (Boolean(memento) && (equals$1(0, config.max) || gt$1(config.max, config.oSet.size)))
      config.oSet.add(toggleObj.__key);
    function replace() {
      const { value: currentKey } = config.oSet.keys().next();
      if (!equals$1(currentKey, toggleObj.__key)) {
        currentKey && config.oSet.delete(currentKey);
        config.oSet.add(toggleObj.__key);
      }
      return true;
    }
    function range(force) {
      const current = toggleObj.isToggled;
      const b = equals$1(void 0, force) ? !current : Boolean(force);
      if (equals$1(current, b))
        return current;
      const sz = config.oSet.size;
      if (b) {
        if (gte$1(sz, config.max))
          return current;
        config.oSet.add(toggleObj.__key);
        return true;
      }
      if (gte$1(config.min, sz))
        return current;
      config.oSet.delete(toggleObj.__key);
      return false;
    }
    target[udpName] = toggleObj;
    return target;
  };
  decorate = Object.defineProperties(decorate, {
    max: {
      configurable: false,
      enumerable: true,
      get: function() {
        return config.max;
      },
      set: function(n) {
        validateMax(n, config.min);
        config.max = n;
      }
    },
    min: {
      configurable: false,
      enumerable: true,
      get: function() {
        return config.min;
      },
      set: function(n) {
        validateMin(n, config.max);
        config.min = n;
      }
    },
    size: {
      configurable: false,
      enumerable: true,
      writeable: false,
      get: function() {
        return config.oSet.size;
      }
    }
  });
  return decorate;
}
export { toggle };
