## Toggle decorator

For certain kinds of applications it may be beneficial to take a standardised approach to the selection and implementation of multiple options, which might include radio buttons, checkboxes, tabs, or accordions, among others.

**toggle()** is a factory function that receives optional configuration and returns a function which will decorate a series of target objects (or classes) with a computed property. The computed property, whose name is based on the configuration, is an object containing:

- the status of the target's toggle (a boolean),
- a toggle() function
- a memento of the target's original toggle status
- the current maximum configured number of true targets
- the current minimum configured number of true targets
- the current number of toggled true targets

The returned decorator function also provides properties which allow run-time configuration inspection and updating:

- max: the maximum number of toggled true targets allowed
- min: the minimum number of toggled true targets allowed
- size: the current number of toggled true targets (not writable)

## Install

The library is written in typescript.
It is distributed with .es and .umd builds and a toggle.d.ts definition file.

```
$ npm install toggle-decorator
```

## Usage

### A Simple StopLight example

```
const [red, green] = [
  {color:'red', on: true },
  {color:'green', on: false}
].map(toggle('on'));

red.__onToggle.isToggled is TRUE;
green.__onToggle.isToggled is FALSE;

green.onToggle.toggle();

red.__onToggle.isToggled is FALSE;
green.__onToggle.isToggled is TRUE;
```

Additional examples can be found in the github repository.

### Configuration

The default configuration, as above, is for a Replacement Toggle (see more below). The name, maximum number of targets, minimum number of targets, and defaultValue are configurable.

- 'name' defaults to 'selected'
- 'max' defaults to 1
- 'min' defaults to 1
- 'defaultValue' defaults to false

Configuration can be either:

- arguments, e.g. toggle('selected',1,1,false)
- **or** options, like toggle({ name: 'selected, max: 1, min: 1, defaultValue: false })

Common sense validation occurs on configuration elements.
Targets are decorated sequentially in the order provided.

When a target object contains a key which matches the configured 'name', the value related to that key will be retained as a memento. When no match is found, the configured 'defaultValue' will be used instead. The memento can be used for reset scenarios.

A target's initial toggle status will use the memento (converted to a Boolean if necessary):

- until the configured 'max' of true targets has been met, then the rest will be set to false
- **or** when the configured 'max' is zero, always

## Types of Toggles:

### Zero:

There may be some occasions where you want to programatically disallow toggling, and the Zero toggle provides for that. No toggling will occur when the max is set to 0. Changing the max will allow toggling:

```
const decorator = toggle({max:0});
(decorator.max is 0)
later...
decorator.max = 10;
```

### Replacement (radios, tabs):

The most typical toggling scenario, replacement toggles have a series of targets where one is always selected. When toggle() is executed on a target whose status is false it is toggled true, and the current target is toggled false. In other words, the minimum number of true targets is one, and the maximum number is also one.

### Range (checkbox set):

The decorator can be configured with minimum and maximum numbers; the minimum must be greater or equal to zero and less than the maximum. For example, a range of 0 (min) - 2 (max) would mean that either zero, one, or two targets could be selected; a range of 3 - 10 would mean that no less than three and no more than 10 could be selected.

### Forcing targets

For Range Toggles, it's convenient to be able to reset a series of targets to a particular value - either all true, or all false - regardless of their current values. The toggle() function provides a 'force' argument which can be employed for this purpose, e.g.:

```
targets.forEach((target) => target.__selectedToggle.toggle(true));
```

Targets which are already true will remain so. The rest will be toggled true until the configured 'max' of true targets has been met.

## Happy Toggling!
